package com.project.ordermanagement.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.project.ordermanagement.dto.OrdersManagementRequestDto;
import com.project.ordermanagement.service.OrderManagementService;


@RestController
public class OrderManagementController {
	
	@Autowired
	private OrderManagementService orderManagementService;
	
	/**
	 * logger.
	 */
	private static final Logger logger = LoggerFactory.getLogger(OrderManagementController.class);
	
	@PostMapping("/stocks")
	public ResponseEntity<String> orderStock(@RequestBody OrdersManagementRequestDto orderManagementRequestDto) {
		logger.debug("inside orderStock() method");
		System.out.println("user id is "+orderManagementRequestDto.getUserId());
		String orderStatus = orderManagementService.orderStocks(orderManagementRequestDto);
		logger.info("order status response "+ orderStatus);
		logger.debug("leaving orderStock() method");
		return new ResponseEntity<String>(orderStatus, HttpStatus.OK);
	}

}
