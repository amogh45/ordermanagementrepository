package com.project.ordermanagement.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CardDetailAndPrice {
	
	
	private String cardNumber;
	private String cvv;
	private Double amount;
	
	

}
