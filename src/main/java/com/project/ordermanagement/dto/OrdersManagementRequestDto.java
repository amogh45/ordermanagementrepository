package com.project.ordermanagement.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class OrdersManagementRequestDto {
	
	private Long userId;
	
	private String stockCode;
	
	private Long quantity;
	
	
	

}
