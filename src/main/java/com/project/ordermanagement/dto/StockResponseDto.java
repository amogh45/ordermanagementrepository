package com.project.ordermanagement.dto;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class StockResponseDto {
	
	
	private Integer stocks_id;
	
	private String stockName;
	private String stockDescription;
	private BigDecimal stockPrice;
	private Date stockDate;
	
	

}
