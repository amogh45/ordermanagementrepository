package com.project.ordermanagement.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name="user_account")
@Data
public class UserAccount {
	
	@Id
	private Long userAccountId;
	
	private String creditCardNumber;
	
	private String cvv;
	
	

}
