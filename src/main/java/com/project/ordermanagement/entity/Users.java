package com.project.ordermanagement.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="users")
@Data
@Setter
@Getter
public class Users {
	
	
	@Id
	private Long userId;
	
	private String userName;
	
	private String password;
	
	private String email;
	
	@OneToOne
	private UserAccount userAccount;

}
