package com.project.ordermanagement.http;


import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

/**
 * @author Emxcel
 * @version 1.0
 * @since 13/1/2020
 */

public interface HttpService {

    Object clientRestCall(String url, Object object, HttpMethod httpMethod, MediaType mediaType, Object mockData, String token);

    Object clientRestCallWithErrorHandling(String url, Object object, HttpMethod httpMethod, MediaType mediaType, String token);
    Object clientRestCallWithErrorHandling(String url, Object object, HttpMethod httpMethod, HttpHeaders httpHeaders);
    Object clientRestCallWithErrorHandlingToVerifyAccessToken(String url, Object object, HttpMethod httpMethod, HttpHeaders httpHeaders);
    void clientRestCallWithoutErrorHandling(String url, Object object, HttpMethod httpMethod, HttpHeaders httpHeaders);
}
