package com.project.ordermanagement.http;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;

@Service
public class HttpServiceImpl implements HttpService {

	@Autowired
	RestTemplate restTemplate;

	@Override
	public Object clientRestCall(String url, Object object, HttpMethod httpMethod, MediaType mediaType, Object mockData,
			String token) {
		return HttpUtil.restCall(url, restTemplate, token, object, httpMethod, mediaType);
	}

	public Object hystrixFallback(String url, Object object, HttpMethod httpMethod, MediaType mediaType,
			Object mockData, String token) {
		return mockData;
	}

	@Override
	public Object clientRestCallWithErrorHandling(String url, Object object, HttpMethod httpMethod, MediaType mediaType,
			String token) {
		try {
			return HttpUtil.restCall(url, restTemplate, token, object, httpMethod, mediaType);
		} catch (RestClientResponseException exception) {
			if(exception.getRawStatusCode() == 401) {
				
			}
			if(exception.getRawStatusCode() == 404) {
				throw new RuntimeException("Information not found");
			}
			throw new RuntimeException( exception.getMessage());
		}
	}

	@Override
	public Object clientRestCallWithErrorHandling(String url, Object object, HttpMethod httpMethod,
			HttpHeaders httpHeaders) {
		try {
			return HttpUtil.restCall(url, restTemplate, object, httpMethod, httpHeaders);
		} catch (RestClientResponseException exception) {
if(exception.getRawStatusCode() == 401) {
				
			}
			if(exception.getRawStatusCode() == 404) {
				throw new RuntimeException("Information not found");
			}
			throw new RuntimeException( exception.getMessage());
		}
	}

    @Override
    public Object clientRestCallWithErrorHandlingToVerifyAccessToken(String url, Object object, HttpMethod httpMethod,
                                                  HttpHeaders httpHeaders) {
        try {
            return HttpUtil.restCall(url, restTemplate, object, httpMethod, httpHeaders);
        } catch (RestClientResponseException exception) {
                throw new RuntimeException("Information not found");
        }
    }
    
    @Override
    public void clientRestCallWithoutErrorHandling(String url, Object object, HttpMethod httpMethod, HttpHeaders httpHeaders) {
        HttpUtil.restCallWithoutResponse(url, restTemplate, object, httpMethod, httpHeaders);
    }
}