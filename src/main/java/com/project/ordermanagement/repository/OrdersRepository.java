package com.project.ordermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.project.ordermanagement.entity.Orders;

@Repository
public interface OrdersRepository extends JpaRepository<Orders, Long> {
	
	

}
