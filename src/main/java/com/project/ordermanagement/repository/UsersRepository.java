package com.project.ordermanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.project.ordermanagement.entity.Users;

@Repository
public interface UsersRepository extends JpaRepository<Users, Long> {
	
	

}
