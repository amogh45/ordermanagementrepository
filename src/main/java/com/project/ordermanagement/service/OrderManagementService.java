package com.project.ordermanagement.service;

import org.springframework.stereotype.Service;

import com.project.ordermanagement.dto.OrdersManagementRequestDto;

@Service
public interface OrderManagementService {

	
	String orderStocks(OrdersManagementRequestDto orderManagementRequestDto);

}
