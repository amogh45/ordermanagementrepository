package com.project.ordermanagement.serviceimpl;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import com.project.ordermanagement.dto.CardDetailAndPrice;
import com.project.ordermanagement.dto.OrdersManagementRequestDto;
import com.project.ordermanagement.dto.StockResponseDto;
import com.project.ordermanagement.entity.Orders;
import com.project.ordermanagement.entity.Users;
import com.project.ordermanagement.exception.UserNotFoundException;
import com.project.ordermanagement.http.HttpService;
import com.project.ordermanagement.repository.OrdersRepository;
import com.project.ordermanagement.repository.UsersRepository;
import com.project.ordermanagement.service.OrderManagementService;

@Service
public class OrderManagementServiceImpl implements OrderManagementService {
	
	
	@Autowired
	private UsersRepository usersReository;
	
	@Autowired
	private OrdersRepository ordersRepository;
	
	@Autowired
	private EurekaClient discoveryClient;
	
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private HttpService httpService;
	
	
	@Autowired
	private ModelMapper mapper ;
	
	
	
	/**
	 * logger.
	 */
	private static final Logger logger = LoggerFactory.getLogger(OrderManagementServiceImpl.class);

	@Override
	public String orderStocks(OrdersManagementRequestDto orderManagementRequestDto) {
		
		logger.info("user id "+ orderManagementRequestDto.getUserId());
		StockResponseDto responseDto = new StockResponseDto();
		InstanceInfo instanceInfo = null;
		Orders orders = new Orders();
		Optional<Users> optionalUsers = usersReository.findById(orderManagementRequestDto.getUserId());
		if (!optionalUsers.isPresent()) {
			throw new UserNotFoundException("users not found");
		}
		//querying eureka server
		instanceInfo = this.discoveryClient.getNextServerFromEureka("STOCKSERVICE", false);
		String stockServiceBaseUrl = instanceInfo.getHomePageUrl();
		logger.info("stock base url "+stockServiceBaseUrl);
		instanceInfo = this.discoveryClient.getNextServerFromEureka("PAYMENT", false);
		String paymentBaseUrl = instanceInfo.getHomePageUrl();
		logger.info("payment base url "+paymentBaseUrl);
		
		// stocks api integration
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		Object responseMap;
		try {
		    responseMap = httpService.clientRestCallWithErrorHandling(stockServiceBaseUrl + "stock/instrument/" + orderManagementRequestDto.getStockCode(), null, HttpMethod.GET, headers);
		    Map<String, Object> map = mapper.map(responseMap, Map.class);
		    Map<String, Object> data = (Map<String, Object>) map.get("data");
		    Map<String, Object> resp = (Map<String, Object>) data.get("stockDetail");
		    orders.setStockCode((String)resp.get("stockName"));
		    orders.setPurchasePrice((Double)resp.get("stockPrice"));
		    
		} catch (Exception e) {
		    logger.info("Error in getting response from stocks api");
		}
		
		// payment api integration
		CardDetailAndPrice request = new CardDetailAndPrice();
		request.setCardNumber(optionalUsers.get().getUserAccount().getCreditCardNumber());
		request.setCvv(optionalUsers.get().getUserAccount().getCvv());
		request.setAmount(1000d);
		ResponseEntity<String> paymentResponse = restTemplate.postForEntity(paymentBaseUrl+"payment/do", request, String.class);
		
		if (paymentResponse.getStatusCodeValue() != 200) {
			throw new RuntimeException("resource not found exception");
		}
		orders.setPurchasedDate(LocalDateTime.now());
		orders.setUserId(optionalUsers.get());
		orders.setQuantity(orderManagementRequestDto.getQuantity());
		
		
		ordersRepository.save(orders);
		return "SUCCESS";
	}

}
